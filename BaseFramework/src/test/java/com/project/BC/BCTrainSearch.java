package com.project.BC;


import java.util.Date;
import java.util.Map;

import com.project.pages.GeneralObjects;
import com.project.pages.TrainsSearchObjects;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.android.AndroidDriver;

public class BCTrainSearch extends TestBase {


	public void enterPlaces(Map<String, String> data , ExtentTest logger ) {
		try{
			ClickSendTextandKeyIn(TrainsSearchObjects.origin, data.get("Origin"), logger);
			ClickSendTextandKeyIn(TrainsSearchObjects.Des, data.get("Des"), logger);
			dropDownSelection(TrainsSearchObjects.Jorclass, TrainsSearchObjects.DropdownValue, data.get("Jorclass"), logger);
			click(TrainsSearchObjects.findButton, logger);
		}catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
		}
	}
	
	public void enterinPhone(Map<String, String> data , ExtentTest logger ) {
		try{
			if(data.get("Browser").equalsIgnoreCase("ANDROIDCHROME")) {
				click("//a[@class='signup']", logger);	
			}
			ClickSendText("//input[@id='display-name']", "test", logger);			
			ClickSendText("//input[@id='email']", "test@test.com", logger);			
			ClickSendText("//input[@id='password']", "Password123", logger);
		
			click("//input[@id='submit-button']", logger);
		}catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
		}
	}
	

}